import 'package:flutter/material.dart';
import '../../constants/appAssets.dart';

class Avatar extends StatelessWidget {
  const Avatar(
    this.url, {
    Key? key,
    this.margin,
    this.border,
    this.radius = 36.0,
  }) : super(key: key);

  final BoxBorder? border;
  final EdgeInsets? margin;
  final double radius;
  final String? url;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: border,
      ),
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
            : NetworkImage(url!),
        radius: radius,
      ),
    );
  }
}
