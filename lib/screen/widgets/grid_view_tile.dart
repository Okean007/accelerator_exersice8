import 'package:flutter/material.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/constants/class_person.dart';
import 'package:project1/screen/widgets/avatar_photo.dart';

import '../../generated/l10n.dart';

class PersonGridTile extends StatelessWidget {
  const PersonGridTile(this.person, {Key? key}) : super(key: key);

  final Person person;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Avatar(
          person.image,
          radius: 60.0,
          margin: const EdgeInsets.only(bottom: 20.0),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      statusText(person.status).toUpperCase(),
                      style: AppStyles.s12w400.copyWith(
                        letterSpacing: 1.5,
                        color: statusPrimary(
                          person.status,
                        ),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      person.name ?? S.of(context).noData,
                      textAlign: TextAlign.center,
                      style: AppStyles.s20w500,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      '${person.species ?? S.of(context).noData}, ${person.gender ?? S.of(context).noData}',
                      style: const TextStyle(
                        color: AppColors.neutral2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}

Color statusPrimary(String? status) {
  if (status == 'Dead') return Colors.red;
  if (status == 'Alive') return const Color(0xff00c48c);
  return Colors.grey;
}

String statusText(String? status) {
  if (status == 'Dead') return S.current.dead;
  if (status == 'Alive') return S.current.alive;
  return S.current.noData;
}
