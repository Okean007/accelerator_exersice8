import 'package:flutter/material.dart';

import '../../constants/appColors.dart';
import '../../constants/appStyles.dart';
import '../../constants/class_person.dart';
import '../../generated/l10n.dart';
import 'avatar_photo.dart';

class PersonListTile extends StatelessWidget {
  const PersonListTile(this.person, {Key? key}) : super(key: key);

  final Person person;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Avatar(
          person.image,
          margin: const EdgeInsets.only(right: 20.0),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      statusText(person.status).toUpperCase(),
                      style: AppStyles.s12w400.copyWith(
                        letterSpacing: 1.5,
                        color: statusPrimary(
                          person.status,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      person.name ?? S.of(context).noData,
                      style: AppStyles.s20w500.copyWith(
                        height: 1.6,
                        leadingDistribution: TextLeadingDistribution.even,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      '${person.species ?? S.of(context).noData}, ${person.gender ?? S.of(context).noData}',
                      style: const TextStyle(
                        color: AppColors.neutral2,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}

Color statusPrimary(String? status) {
  if (status == 'Dead') return Colors.red;
  if (status == 'Alive') return const Color(0xff00c48c);
  return Colors.grey;
}

String statusText(String? status) {
  if (status == 'Dead') return S.current.dead;
  if (status == 'Alive') return S.current.alive;
  return S.current.noData;
}
