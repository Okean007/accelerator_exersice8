import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project1/block/person/events.dart';
import 'package:project1/block/person/states.dart';
import 'package:project1/repo/repo_person.dart';

class BlocPersons extends Bloc<EventBlocPersons, StateBlocPersons> {
  BlocPersons({
    required this.repo,
  }) : super(StatePersonsInitial()) {
    on<EventPersonsFilterByName>(
      (event, emit) async {
        emit(StatePersonsLoading());
        final result = await repo.filterByName(event.name);
        if (result.errorMessage != null) {
          emit(
            StatePersonsError(result.errorMessage!),
          );
          return;
        }
        emit(
          StatePersonsData(data: result.personsList!),
        );
      },
    );
  }

  final RepoPersons repo;
}
